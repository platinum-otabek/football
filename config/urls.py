from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
    openapi.Info(
        title="Football API",
        default_version='v1',
        description="Football",
        license=openapi.License(name="Football"),
    ),
    public=True,
    
    authentication_classes=[],
    url='https://localhots:8000/',
)


from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    
     path('swagger/', schema_view.with_ui('swagger',
                                                       cache_timeout=10), name='schema-swagger-ui'),

    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    path('api/stadium/',include('stadium.urls')),
    path('api/order/',include('orders.urls'))

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

