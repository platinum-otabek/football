from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


class CustomUser(AbstractUser):
    ROLE_USERS = (
        ('admin','Admin'),
        ('stadium_owner','Maydon egalari'),
        ('user','User')
    )
    role = models.CharField(max_length=31,choices=ROLE_USERS)
    
    class Meta:
        db_table = 'user'