from rest_framework import permissions

class AccountUserPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated and (request.user.role=='user' or request.user.role=='admin') : 
            return True