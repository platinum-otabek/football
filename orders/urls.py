from django.urls import path
from .views import CreateOrderView,ListOderView,DeleteOrderView

urlpatterns = [
    path('',CreateOrderView.as_view()),
    path('all-orders/',ListOderView.as_view()),
    path('<int:pk>/',DeleteOrderView.as_view())
]