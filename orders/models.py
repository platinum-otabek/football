from django.db import models
from stadium.models import StadiumModels
from account.models import CustomUser
# Create your models here.
class OrderModel(models.Model):
    stadium = models.ForeignKey(StadiumModels,on_delete=models.CASCADE)
    begin_time = models.DateTimeField()
    end_time = models.DateTimeField()
    client = models.ForeignKey(CustomUser,on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.stadium.name
    
    class Meta:
        db_table = 'order'