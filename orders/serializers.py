from rest_framework import serializers
from .models import OrderModel
from django.db.models import Q
import time

class CreateOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderModel
        fields = ('stadium','begin_time','end_time')

    def create(self, validated_data):
        stadium = validated_data.get('stadium')
        begin_time = validated_data.get('begin_time')
        end_time = validated_data.get('end_time')
        client = self.context['request'].user
        validated_data['client']=client
        print(begin_time,int(begin_time.timestamp()*1000))
        if begin_time and end_time and begin_time.timestamp() > int(time.time()) :    
            existing_records = OrderModel.objects.filter(
                Q(stadium=stadium)&
                (Q(begin_time__range = (begin_time, end_time)) |
                Q(end_time__range = (begin_time, end_time)))
            )
            print(11,existing_records)
            if existing_records.exists():
                raise serializers.ValidationError("Bunday vaqtda band")
            instance = OrderModel.objects.create(**validated_data)
            return instance
        
        raise serializers.ValidationError("Iltimos begin time or end time to`g`ri kiriting")

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderModel
        fields = ('id','stadium','begin_time','end_time','client')