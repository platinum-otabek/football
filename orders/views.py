from rest_framework.generics import CreateAPIView,DestroyAPIView,ListAPIView
from .models import OrderModel
from .serializers import CreateOrderSerializer,OrderSerializer
from rest_framework.permissions import IsAuthenticated
from stadium.permissions import StadiumPermissions,IsOwnerOfObjectStadium
from account.permissions import AccountUserPermissions
import datetime

class CreateOrderView(CreateAPIView):
    queryset = OrderModel.objects.all()
    serializer_class = CreateOrderSerializer
    permission_classes = [AccountUserPermissions]

class ListOderView(ListAPIView):
    queryset = OrderModel.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [StadiumPermissions]
    def get_queryset(self):
        queryset = OrderModel.objects.filter(stadium__owner=self.request.user.id,begin_time__gt=datetime.datetime.now())
        return queryset

class DeleteOrderView(DestroyAPIView):
    queryset = OrderModel.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [IsOwnerOfObjectStadium] 
    