from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from account.models import CustomUser


class StadiumModels(models.Model):
    name = models.CharField(max_length=200,default='Stadion',verbose_name='Stadion nomi')
    address = models.CharField(max_length=200,default='')
    contact = models.CharField(max_length=13,default='')
    price = models.PositiveSmallIntegerField(default=1)
    point = models.PointField(geography=True, default=Point(0.0, 0.0))
    owner = models.ForeignKey(CustomUser,on_delete=models.SET_NULL,null=True)

    def __str__(self) -> str:
        return self.name
    
    class Meta:
        db_table = 'stadium'

class StadiumImagesModel(models.Model):
    stadium = models.ForeignKey(StadiumModels,on_delete=models.CASCADE)
    image = models.ImageField(upload_to='products')

    def __str__(self):
        return "%s" % (self.product.name)
    
    class Meta:
        db_table = 'stadium_images'