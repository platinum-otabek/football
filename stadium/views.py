from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.generics import CreateAPIView,RetrieveUpdateAPIView,RetrieveAPIView,ListAPIView
from .serializers import StadiumCreateSerializers,StadiumUpdateSerializers,StadiumAboutSerializer
from .permissions import StadiumPermissions
from .models import StadiumModels
from django.db.models import Q,Subquery,OuterRef
from orders.models import OrderModel
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point



class StadiumCreateView(CreateAPIView):
    parser_class = [MultiPartParser, FormParser]
    serializer_class = StadiumCreateSerializers
    permission_classes = [StadiumPermissions]

class StadiumUpdateView(RetrieveUpdateAPIView):
    queryset = StadiumModels.objects.all()
    serializer_class = StadiumUpdateSerializers
    permission_classes = [StadiumPermissions]    
    lookup_field = 'pk'

class StadiumDetailView(RetrieveAPIView):
    queryset = StadiumModels.objects.all()
    serializer_class = StadiumAboutSerializer

class StadiumListView(ListAPIView):
    queryset = StadiumModels.objects.all()
    serializer_class = StadiumAboutSerializer

    def get_queryset(self):
        
        begin_time = self.request.query_params.get('begin_time')
        end_time = self.request.query_params.get('end_time')
        input_latitude = float(self.request.query_params.get('latitude'))
        input_longitude = float(self.request.query_params.get('longitude'))

        
        input_point = Point(input_longitude, input_latitude, srid=4326)
        
        order_subquery = OrderModel.objects.filter(
            Q(begin_time__range=(begin_time, end_time)) |
            Q(end_time__range=(begin_time, end_time)),
            stadium=OuterRef('pk')
        ).values('stadium').distinct()

        queryset = StadiumModels.objects.annotate(
            distance=Distance('point', input_point)
        ).exclude(
            pk__in=Subquery(order_subquery)
        ).order_by('distance')

        return queryset