from django.urls import path
from .views import StadiumCreateView,StadiumUpdateView,StadiumDetailView,StadiumListView


urlpatterns = [
    path('new-stadium/',StadiumCreateView.as_view()),
    path('update-stadium/<int:pk>/',StadiumUpdateView.as_view()),
    path('<int:pk>/',StadiumDetailView.as_view()),
    path('list/',StadiumListView.as_view())
    
]