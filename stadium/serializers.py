from rest_framework import serializers
from .models import StadiumImagesModel,StadiumModels
from django.contrib.gis.geos import Point


class StadiumImageSerializers(serializers.ModelSerializer):
    

    class Meta:
        model = StadiumImagesModel
        fields = '__all__'

   

class StadiumCreateSerializers(serializers.ModelSerializer):
    images =  StadiumImageSerializers(many=True, read_only=True)
    uploaded_images = serializers.ListField(
        child=serializers.ImageField(allow_empty_file=False, use_url=False),
        write_only=True
    )
    latitude = serializers.FloatField()
    longitude = serializers.FloatField()

    class Meta:
        model = StadiumModels
        fields = ["id", "name", "address", "price", "contact","latitude","longitude",
                   "images","uploaded_images","owner"]

    def create(self, validated_data):
        latitude = validated_data.pop('latitude')
        longitude = validated_data.pop('longitude')
        
        uploaded_images = validated_data.pop("uploaded_images")
        point = Point(longitude, latitude)
        validated_data['point'] = point
        validated_data['owner']=self.context['request'].user
        stadium = StadiumModels.objects.create(**validated_data)
        
        stadium.latitude = latitude
        stadium.longitude = longitude

        for image in uploaded_images:
            StadiumImagesModel.objects.create(stadium=stadium, image=image)

        return stadium
    
class StadiumUpdateSerializers(serializers.ModelSerializer):
    images =  StadiumImageSerializers(many=True, read_only=True)
    uploaded_images = serializers.ListField(
        child=serializers.ImageField(allow_empty_file=False, use_url=False),
        write_only=True
    )
    latitude = serializers.FloatField(required=False)
    longitude = serializers.FloatField(required=False)

    class Meta:
        model = StadiumModels
        fields = ["id", "name", "address", "price", "contact","latitude","longitude",
                   "images","uploaded_images"]

    
class StadiumAboutSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()
    
    class Meta:
        model= StadiumModels
        fields = ('id','name','address','price','contact','point','images') 
    
    def get_images(self, obj):
        request = self.context.get('request')
        images = StadiumImagesModel.objects.filter(stadium=obj)
        image_paths = [request.build_absolute_uri(image.image.url) for image in images]
        return image_paths
    