from rest_framework import permissions

class StadiumPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated and (request.user.role=='admin' or request.user.role=='stadium_owner') : 
            return True
    
class IsOwnerOfObjectStadium(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        print(obj,request.user)
        return obj == request.user and request.user.is_authenticated